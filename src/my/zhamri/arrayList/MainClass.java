package my.zhamri.arrayList;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MainClass {

    Student student;
    List<Student> studentList = new ArrayList<>();
    List<Student> studentListClone = new ArrayList<>();

    public static void main(String[] args) {
        try {
            new MainClass().Main();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void Main() throws IOException {


        readTextFile("MyData.txt");
        displayAll();
        appendStudent("MyData.txt");
        readTextFile("MyData.txt");
        displayAll();


//        addStudent("123", "Zhamri", "MAS");
//        addStudent("456", "Ali", "TNB");
//        addStudent("789", "Kamal", "PROTON");
//        displayAll();
//
//        updateTextFile();

//        myMain.replaceStudent("789");
//        myMain.displayAll();
//
//        myMain.removeStudent("456");
//        myMain.displayAll();
//
//        myMain.findById("789");

    }

    private void addStudent(String id, String name, String inasis) {
        student = new Student(id, name, inasis);
        studentList.add(student);
    }

    private void displayAll() {
        for (Student student : studentList) {
            System.out.println(student.getId() + " " + student.getName() + " " + student.getInasis());
        }
    }

    private void updateTextFile() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("MyData.txt", "UTF-8");
            for (Student student : studentList) {
                writer.println(student.getId());
                writer.println(student.getName());
                writer.println(student.getInasis());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            writer.close();

        }
    }

    private void removeStudent(String id) {
        for (Student student : studentList) {
            if (student.getId().equalsIgnoreCase(id))
                studentList.remove(student);
        }
    }

    private void replaceStudent(String id) {
        int index = 0;
        for (Student student : studentList) {
            if (student.getId().equalsIgnoreCase(id)) {
                student.setName("Ibu");
                studentList.set(index, student);
            }
            index++;
        }
    }

    private void displayClone() {
        for (Student student : studentListClone) {
            System.out.println(student.getId() + " " + student.getName() + " " + student.getInasis());
        }
    }

    public void findById(String id) {
        for (Student student : studentList) {
            if (student.getId().equals(id)) {
                studentListClone.add(student);
            }
        }
        displayClone();
    }

    private void readTextFile(String whatFile) throws IOException {
        studentList = new ArrayList<>();
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(whatFile));

            String id = br.readLine();
            while (id != null) {
                String name = br.readLine();
                String inasis = br.readLine();
                student = new Student(id, name, inasis);
                studentList.add(student);
                id = br.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    public void appendStudent(String whatFile) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {


            File file = new File(whatFile);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            String id = "777\n";
            String name = "Misbun\n";
            String inasis = "PERNAS";

            bw.write("\n");
            bw.write(id);
            bw.write(name);
            bw.write(inasis);

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }

    }

}
