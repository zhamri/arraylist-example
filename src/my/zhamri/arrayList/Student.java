package my.zhamri.arrayList;

public class Student {
    private String id;
    private String name;
    private String inasis;

    public Student(String id, String name, String inasis) {
        this.id = id;
        this.name = name;
        this.inasis = inasis;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInasis() {
        return inasis;
    }

    public void setInasis(String inasis) {
        this.inasis = inasis;
    }
}
